class HashStorage {
  constructor() {
    this.store = {}; 
  }

  addValue(key, value) {
    this.store[key] = value;
    return this.store[key];

  }

  getValue(key) {
     if(key in this.store) {
      return this.store[key];
    } else {
      return false;
     }
  }

  deleteValue(key) {
    if(key in this.store) {
      return delete this.store[key];
    } else {
      return false;
     }
  }

  getKeys() {
    return Object.keys(this.store);
  }
}

const coctailsStorage = new HashStorage();

coctailsStorage.addValue('latte', {alcohol: "нет", recepy: "djjdjdjjd"});
coctailsStorage.addValue('americano', {alcohol: "нет", recepy: "djjdjdjjd"});
coctailsStorage.addValue('espresso', {alcohol: "нет", recepy: "djjdjdjjd"});
coctailsStorage.addValue('cappuccino', {alcohol: "нет", recepy: "djjdjdjjd"});
